## Added Variables:

`DEBRICKED_TOKEN` - Token from debricked.com

`PRIVATE_TOKEN` - Personal token on gitlab.com with api scope for creating issues

`PROJECT_ID` - The projects ID found on https://gitlab.com/just26/devops-home-task/, used for creating issues. Couldn't decided if I wanted this inside the .gitlab-ci.yml file or not, but ultimately decided not to put it in the file.


## Gitlab settings:

Currently, my settings aren't like this, due to me being the only one to work at this project, but I would set it up something like this.

- Pipelines must succeed for merge checks, and skipped 
- Prevent approvals from user who add commit
- set a group of users that work on the project to be able to approve MRs
- require at least 1 approver for the main / master branch(es)
- if main / master branch(es) aren't protected, set them as protected and set that no one can directly push to them
- create feature/*  protected branch, had to do this to be able to get debricked to run on these branches (I think, I also disconnected and connected the integration with debricked.com again at the same time, so not sure which of the 2 that helped) 

## Completed tasks:

Creating a pipeline containing the following:
- Debricked's testing tool
- Unit test (the supplied one)
- Building a docker container, and placing it in gitlab's registry -> registry.gitlab.com/just26/devops-home-task:latest
- Creating issue with a simple title and description in case of failure in pipeline. I decided not to spend too much time in creating a well written description, and instead just showed that I know how to access the different fields (topic, description) and put some values, an example is here: https://gitlab.com/just26/devops-home-task/-/issues/7 
- Running a scan as soon as something is pushed or merged
- Create a deploy section in the pipeline for GKE
- Created a LoadBalancer service and connected it to the deployment
- Created a undeploy section in the pipeline to


Outside pipeline:
- Slimmed down the docker file with .dockerignore, only adding files needed.
- Setup GKE cluster manually GCP website
- Setup agent for GKE (via .gitlab/agents/<agent name>)
- Connect GKE with the agent, with the command `helm upgrade --install gke-agent gitlab/gitlab-agent     --namespace gitlab-agent     --create-namespace     --set image.tag=v15.1.0     --set config.token=<token>     --set config.kasAddress=wss://kas.gitlab.com`


## Issues I ran into:

- If I create a new file, commit it to a new branch, and let the pipeline run it, I run into the following issue with Debricked's tool: https://gitlab.com/just26/devops-home-task/-/jobs/2609827267. This issue doesn't happen when there's a commit directly to main, then the pipeline goes through the entire way. I tried finding any issues in your documentation about this, but didn't seem to find any. Same goes with MRs


## Rough time estimate:
- 1 hour Dockerfile, .dockerignore, verifying unit test.
- 2 hours building up the foundation of the pipeline.
- 4 hours trying to get GKE integration to work.
- 1 hour troubleshooting Debricked integration why it didn't work all the time
- 2 hours after a dinner break and some relaxing, came back and managed to fix the GKE integration. Biggest issue, didn't find the correct documentation
- 2 hours fiddling with some commit/mr pipeline settings

## Pipeline times

I'm quite happy with the speed of the pipeline, it's about 3 minutes for what it currently is.


## Final thoughts:

It was a great little project to spend my saturday on. I wish I had enough knowledge of GKE and how to integrate the pipeline to it, or at least finding the documentation for it instead of spending half the estimated time for this task.

