#Use NODE version 16 (LTS) base image
FROM node:16

#Set the working dir in docker
WORKDIR /opt/devops-home-task

#get / install the dependencies
COPY package*.json ./

#Build
RUN npm ci --omit=dev

#Bundle the source
COPY . .

#Open port 1337
EXPOSE 1337

#Run the application
CMD [ "node", "app.js" ]
